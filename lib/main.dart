import "package:flutter/material.dart";
import "./app_screens/home.dart";

void main() {
  runApp(MaterialApp(
    title: "Exploring Widget",
    home: Scaffold(
      appBar: AppBar(
        title: Text("ListView Example"),
      ),
      body: getListView_2(),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          tooltip: "Basic Clicked",
          onPressed: () {
            debugPrint("FloatingAction clicked");
          }),
    ),
  ));
}

List<String> getListElements() {
  var items = List<String>.generate(1000, (counter) => "Item $counter");
  return items;
}

Widget getListView_2() {
  List<String> listItems = getListElements();

  var listView = ListView.builder(itemBuilder: (context, index) {
    return ListTile(
      leading: Icon(Icons.arrow_right),
      title: Text(listItems[index]),
      onTap: () {
        showSnackbar(context, listItems[index]);
      },
    );
  });
  return listView;
}

void showSnackbar(BuildContext context, String item) {
  SnackBar snackbar = SnackBar(
    content: Text(item),
    action: SnackBarAction(
        label: "UNDO",
        onPressed: () {
          debugPrint("Undo Dummy Operation");
        }),
  );

  Scaffold.of(context).showSnackBar(snackbar);
}

Widget getListView() {
  ListView listView = ListView(
    children: <Widget>[
      ListTile(
        leading: Icon(Icons.landscape),
        title: Text("ListTile Page One"),
        subtitle: Text("Subtitle of Page One"),
        trailing: Icon(Icons.wb_sunny),
        onTap: () {
          debugPrint("Page One Clicked");
        },
      ),
      ListTile(
        leading: Icon(Icons.phone),
        title: Text("ListTile Page Two"),
      ),
      ListTile(
        leading: Icon(Icons.android),
        title: Text("ListTile Page Three"),
      ),
      Text("Text inside List View"),
      Container(
        color: Colors.red,
        height: 50.0,
      )
    ],
  );

  return listView;
}
